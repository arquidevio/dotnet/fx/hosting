module Arquidev.Fx.Hosting.Tests

open Arquidev.Fx.Hosting.Core
open Expecto

[<Tests>]
let tests =
    testList "fix me" [ testTask "fix me" { "Fix me" |> Expect.equal true false } ]
