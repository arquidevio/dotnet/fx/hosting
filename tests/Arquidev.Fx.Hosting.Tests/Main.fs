﻿namespace Arquidev.Fx.Hosting

open Expecto

module TestApp =

    [<EntryPoint>]
    let main argv =
        Tests.runTestsInAssembly defaultConfig argv
