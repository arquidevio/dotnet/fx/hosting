using Arquidev.Fx.Hosting.Metadata;
using Arquidev.Fx.Hosting.StartupHooks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Hosting.Internal;

namespace Arquidev.Fx.Hosting.Core;

public static class Extensions
{
    private const string AppNameEnvVarName = "APP";
    private const string ContainerImageEnvVarName = "CONTAINER_IMAGE";
    private const string ContainerNameEnvVarName = "HOSTNAME";
    private const string OriginCommitHashEnvVarName = "COMMIT_HASH";
    private const string OriginSourceRepoEnvVarName = "SOURCE_REPO_URL";

    private static ContainerImage GetDockerImageData(string envVarPrefix)
    {
        var dockerImageValue = Environment.GetEnvironmentVariable(envVarPrefix + ContainerImageEnvVarName);
        var originCommitHash = Environment.GetEnvironmentVariable(envVarPrefix + OriginCommitHashEnvVarName);
        var originSourceRepoUrl = Environment.GetEnvironmentVariable(envVarPrefix + OriginSourceRepoEnvVarName);
        if (dockerImageValue is null)
            return new ContainerImage(null, null, originSourceRepoUrl, originCommitHash);
        var chunks = dockerImageValue.Split(":");
        return new ContainerImage(string.Join(":", chunks[..^1]), chunks[^1], originSourceRepoUrl, originCommitHash);
    }

    private static ContainerRuntimeData GetKubernetesRuntimeData(string envVarPrefix)
    {
        static string? ReadText(string filePath) => File.Exists(filePath) ? File.ReadAllText(filePath) : null;
        var containerId = Environment.GetEnvironmentVariable(envVarPrefix + ContainerNameEnvVarName);
        var podName = ReadText("/etc/podinfo/name");
        var podNamespace = ReadText("/etc/podinfo/namespace");
        var podLabels = ReadText("/etc/podinfo/labels") is { } labels
            ? from label in labels.Split(Environment.NewLine)
              let chunks = label.Split("=", 2)
              select new KeyValuePair<string, string>(chunks[0], chunks[1].Trim('"'))
            : [];
        var kubernetes = podName is not null && podNamespace is not null
            ? new KubernetesRuntimeData(podName, podNamespace, podLabels)
            : null;
        var appName = Environment.GetEnvironmentVariable(envVarPrefix + AppNameEnvVarName);

        return new ContainerRuntimeData(containerId, appName, kubernetes);
    }

    /// <summary>
    /// Includes an optional additional json file intended to hold secrets
    /// </summary>
    /// <param name="builder">Configuration builder</param>
    /// <param name="filePath">Enables overriding file path.</param>
    /// <param name="optional">Whether the file is optional.</param>
    /// <returns></returns>
    public static IConfigurationBuilder AddSecretsJsonFile(this IConfigurationBuilder builder, string filePath = "appsettings.secrets.json",
        bool optional = true) =>
        builder.AddJsonFile(filePath, optional);

    public static IHostBuilder ConfigureDefaults(this IHostBuilder hostBuilder, string envVarPrefix = "") =>
        hostBuilder
            .ConfigureHostConfiguration(cb => cb.AddEnvironmentVariables(envVarPrefix))
            .ConfigureServices((ctx, services) =>
            {
                ctx.SetAppMetadata(new AppMetadata(GetDockerImageData(envVarPrefix), GetKubernetesRuntimeData(envVarPrefix)));
                services.AddSingleton<ConsoleLifetime>();
                services.AddSingleton<IHostLifetime, StartupHookEnabledConsoleLifetime>();
                services.AddOptions<StartupActions>();
                services.Configure<ConsoleLifetimeOptions>(options => options.SuppressStatusMessages = true);
                services.Configure<HostOptions>(options => options.ShutdownTimeout = TimeSpan.FromSeconds(15));
                if (ctx.Configuration.GetValue<string>("config_dump") is not null)
                {
                    services.OnBeforeHostStarted(0, "config-dump", (f, _) =>
                    {
                        var cfg = f.GetRequiredService<IConfiguration>();
                        Console.WriteLine((cfg as IConfigurationRoot)!.GetDebugView());
                        Environment.Exit(0);
                        return Task.CompletedTask;
                    });
                }
            });
}
