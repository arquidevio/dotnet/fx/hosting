using Arquidev.Fx.Hosting.Metadata;

namespace Arquidev.Fx.Hosting.Core;

public class AppMetadata : IAppMetadata
{
    public AppMetadata(ContainerImage image, ContainerRuntimeData container)
    {
        Image = image;
        Container = container;
    }

    public ContainerImage Image { get; }
    public ContainerRuntimeData Container { get; }
}
