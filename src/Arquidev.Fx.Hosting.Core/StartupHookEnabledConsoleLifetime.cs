﻿using Arquidev.Fx.Hosting.StartupHooks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Hosting.Internal;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Arquidev.Fx.Hosting.Core;

/// <summary>
/// Specialised <see cref="ConsoleLifetime"/> enabling registering and running arbitrary actions
/// on before <see cref="IHost"/> gets started.
/// </summary>
public class StartupHookEnabledConsoleLifetime(IOptions<StartupActions> startupActions,
    ILogger<StartupHookEnabledConsoleLifetime> logger,
    IServiceProvider serviceProvider,
    ConsoleLifetime consoleLifetime) : IHostLifetime
{
    private readonly StartupActions _startupActions = startupActions.Value;
    private readonly ILogger<StartupHookEnabledConsoleLifetime> _logger = logger;
    private readonly IServiceProvider _serviceProvider = serviceProvider;
    private readonly ConsoleLifetime _consoleLifetime = consoleLifetime;

    public async Task WaitForStartAsync(CancellationToken cancellationToken)
    {
        _logger.LogDebug("Running on before host startup hook actions");
        foreach (var (priority, name, action) in _startupActions.Actions.OrderBy(x => x.Priority))
        {
            _logger.LogDebug("Running action: {HookName} (Priority: {Priority})", name, priority);
            await action(_serviceProvider, cancellationToken);
        }
        _logger.LogDebug("Host startup hook actions executed");
        await _consoleLifetime.WaitForStartAsync(cancellationToken);
    }

    public Task StopAsync(CancellationToken cancellationToken) => _consoleLifetime.StopAsync(cancellationToken);
}
