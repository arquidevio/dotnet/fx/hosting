namespace Arquidev.Fx.Hosting.Metadata;

public static class CommonLabels
{
    public const string ImageVersion = "image_version";
    public const string ImageName = "image_name";
    public const string CommitHash = "commit_hash";
    public const string SourceRepoUrl = "source_repo_url";
    public const string ContainerId = "container_id";
    public const string App = "app";
    public const string KubernetesPod = "pod";
    public const string KubernetesNamespace = "namespace";
}

public record ContainerImage(
    string? Name,
    string? Tag,
    string? OriginRepositoryUrl,
    string? OriginCommitHash
);

public record KubernetesRuntimeData(
    string PodName,
    string PodNamespace,
    IEnumerable<KeyValuePair<string, string>> PodLabels
);

public record ContainerRuntimeData(
    string? ContainerId,
    string? App,
    KubernetesRuntimeData? Kubernetes
);

public interface IAppMetadata
{
    ContainerImage Image { get; }
    ContainerRuntimeData Container { get; }
}
