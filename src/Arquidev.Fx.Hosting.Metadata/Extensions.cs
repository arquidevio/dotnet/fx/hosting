using Microsoft.Extensions.Hosting;

namespace Arquidev.Fx.Hosting.Metadata;

public static class Extensions
{
    private const string MetadataKey = "Arquidev.Fx.Hosting.Metadata";
    public static IAppMetadata? GetAppMetadata(this HostBuilderContext ctx)
        => ctx.Properties.TryGetValue(MetadataKey, out var value) && value is IAppMetadata metadata ? metadata : null;

    public static void SetAppMetadata(this HostBuilderContext ctx, IAppMetadata metadata)
        => ctx.Properties[MetadataKey] = metadata;
}
