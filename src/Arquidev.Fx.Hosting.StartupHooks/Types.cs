﻿using Microsoft.Extensions.DependencyInjection;

namespace Arquidev.Fx.Hosting.StartupHooks;

public sealed record StartupAction(int Priority, string Name, Func<IServiceProvider, CancellationToken, Task> Action);

public class StartupActions
{
    public List<StartupAction> Actions { get; } = new();
}

public static class Extensions
{
    /// <summary>
    /// Registers an action to be executed before the host has started. Can be called multiple times.
    /// The order in which actions execute is determined by the <see cref="priority"/> - lower numbers run first.
    /// </summary>
    /// <remarks>All the registered actions run before any IHostedService instances.</remarks>
    /// <param name="services">The <see cref="IServiceCollection"/> to add the services to.</param>
    /// <param name="priority">Action priority.</param>
    /// <param name="name">Action friendly name.</param>
    /// <param name="action">Action to be executed.</param>
    /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
    public static IServiceCollection OnBeforeHostStarted(this IServiceCollection services, int priority,
        string name, Func<IServiceProvider, CancellationToken, Task> action) =>
        services.Configure<StartupActions>(x =>
            x.Actions.Add(new StartupAction(priority, name, action)));

    /// <summary>
    /// Registers an action to be executed before the host has started but after all the other already registered actions.
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection"/> to add the services to.</param>
    /// <param name="name">Action friendly name.</param>
    /// <param name="action">Action to be executed.</param>
    /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
    public static IServiceCollection OnBeforeHostStarted(this IServiceCollection services,
        string name, Func<IServiceProvider, CancellationToken, Task> action) =>
        services.Configure<StartupActions>(x =>
            x.Actions.Add(new StartupAction((x.Actions.Count > 0 ? x.Actions.Max(a => a.Priority) : 0) + 100, name,
                action)));

    /// <summary>
    /// Registers an action to be executed before the host has started but after all the other already registered actions.
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection"/> to add the services to.</param>
    /// <param name="name">Action friendly name.</param>
    /// <param name="action">Action to be executed.</param>
    /// <typeparam name="T">Service type</typeparam>
    /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
    public static IServiceCollection OnBeforeHostStarted<T>(this IServiceCollection services,
        string name, Func<T, CancellationToken, Task> action) where T : notnull =>
        services.OnBeforeHostStarted(name, (f, token) =>
            action(f.GetRequiredService<T>(), token));

    /// <summary>
    /// Registers an action to be executed before the host has started but after all the other already registered actions.
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection"/> to add the services to.</param>
    /// <param name="name">Action friendly name.</param>
    /// <param name="action">Action to be executed.</param>
    /// <typeparam name="T1">Service type</typeparam>
    /// <typeparam name="T2">Service type</typeparam>
    /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
    public static IServiceCollection OnBeforeHostStarted<T1, T2>(this IServiceCollection services,
        string name, Func<T1, T2, CancellationToken, Task> action)
        where T1 : notnull where T2 : notnull =>
        services.OnBeforeHostStarted(name, (f, token) =>
            action(f.GetRequiredService<T1>(), f.GetRequiredService<T2>(), token));

    /// <summary>
    /// Registers an action to be executed before the host has started but after all the other already registered actions.
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection"/> to add the services to.</param>
    /// <param name="name">Action friendly name.</param>
    /// <param name="action">Action to be executed.</param>
    /// <typeparam name="T1">Service type</typeparam>
    /// <typeparam name="T2">Service type</typeparam>
    /// <typeparam name="T3">Service type</typeparam>
    /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
    public static IServiceCollection OnBeforeHostStarted<T1, T2, T3>(this IServiceCollection services,
        string name, Func<T1, T2, T3, CancellationToken, Task> action)
        where T1 : notnull where T2 : notnull where T3 : notnull =>
        services.OnBeforeHostStarted(name, (f, token) =>
            action(f.GetRequiredService<T1>(), f.GetRequiredService<T2>(), f.GetRequiredService<T3>(), token));
}
