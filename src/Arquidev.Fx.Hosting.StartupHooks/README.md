# Hosting Startup Hooks

Enables registering actions to be run on before IHost is started.

:warning: This package doesn't run anything on its own. For the actions to actually
execute you'll need the Arquidev.Fx.Hosting.Core package added to your application.
